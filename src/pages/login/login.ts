import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  mail:string;
  password:string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public afAuth: AngularFireAuth) {
  }

  irSingup() {
    this.navCtrl.setRoot('RegisterPage');
  }

  async login() {
    let user = {
      'mail': this.mail,
      'password': this.password,
    };
    try {
      const result = this.afAuth.auth.signInWithEmailAndPassword(user.mail, user.password);
      if(result){
        this.navCtrl.setRoot('RootPage')
      }
    } catch (e) {
      console.log(e)
    }
  }
}
