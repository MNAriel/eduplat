import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  datos_user = [];
  mail;
  cards: any;
  category: string = 'gear';
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private toast: ToastController,
              private afAuth: AngularFireAuth,
              public afDB: AngularFireDatabase) {
    this.cards = new Array(10);
  }

  ionViewDidLoad() {
    this.afAuth.authState.subscribe(data => {
      if(data.email && data.uid) {
        this.toast.create({
          message:`Bienvenido a EduPlat, ${data.email}`,
          duration: 3000
        }).present();
        this.getUser(data.uid).valueChanges().subscribe( user =>{
          this.datos_user = user;
          this.mail = data.email
          console.log(this.datos_user)
        } )
      }
      else {
        this.toast.create({
          message: 'Fallo en la autenticacion',
          duration: 3000
        }).present();
      }
    })
  }

  getUser(id) {
    return this.afDB.list(`profile/${id}`);
  }

}
