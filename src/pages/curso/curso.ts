import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams, ToastController, Platform, LoadingController} from 'ionic-angular';

import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';

import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-curso',
  templateUrl: 'curso.html',
})
export class CursoPage {

  nombre:string = "";
  apellido:string = "";

  name_course:string = "";
  desp_course:string = "";
  nivel_course:string = "";
  imgPreview:string = null;
  img:string = "";

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private platform: Platform,
              private loadingCtrl: LoadingController,
              private imagePicker: ImagePicker,
              private userServ: UserServiceProvider) {
              this.nombre = this.navParams.get("nombre");
              this.apellido = this.navParams.get("apellido");
  }

  private mostar_toast( texto:string ){
    this.toastCtrl.create({
      message:texto,
      duration: 2500
    }).present();
  }

  atras() {
    this.viewCtrl.dismiss();
  }

  crear_publicacion(){
    //console.log(this.myForm.value.titulo);
    //console.log("Subiendo imagen...");
    console.log(this.nombre)
    let archivo = {
      'name_course': this.name_course,
      'desp_course': this.desp_course,
      'nivel_course': this.nivel_course,
      'teacher_name': this.nombre,
      'teacher_last': this.apellido,
      'img': this.img
    };
    console.log("let archivo ");
    console.log(archivo)

    let loader = this.loadingCtrl.create({
      content: "Subiendo..."
    });
    loader.present();
    
    this.userServ.cargar_imagenes_firebase( archivo ). then(
      ()=>{
        loader.dismiss();
        this.atras()
      },

      ( error )=>{
        loader.dismiss();
        this.mostar_toast("Error al cargar: " + error );
        console.log("Error al cargar " + JSON.stringify(error) );
      }

     )

  }

  
  seleccionar_fotos(){

    if( !this.platform.is("cordova") ){
      this.mostar_toast("Error: No estamos en un celular");
      return;
    }

    let opciones: ImagePickerOptions = {
      maximumImagesCount: 1,
      quality: 40,
      outputType: 1
    }


    this.imagePicker.getPictures(opciones).then((results) => {


      for( let img of results ){
        this.imgPreview = 'data:image/jpeg;base64,' + img
        this.img = img;
        break;
      }


    }, (err) => {

      this.mostar_toast("Error seleccion:" + err);
      console.error(  "Error en seleccion: " + JSON.stringify( err ) );

    });

  }
}
