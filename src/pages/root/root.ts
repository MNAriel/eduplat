import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the RootPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-root',
  templateUrl: 'root.html'
})
export class RootPage {

  aprendeRoot = 'AprendePage'
  inicioRoot = 'InicioPage'
  noticiasRoot = 'NoticiasPage'


  constructor(public navCtrl: NavController) {}

}
