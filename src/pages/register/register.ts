import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  grade:string;
  mail:string;
  password:string;
  conf_password:string;
  name_user:string;
  last_name_user:string;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public afAuth: AngularFireAuth,
              public afDB: AngularFireDatabase) {
  }
  //async register_user( user:userDates ){
    async register_user(){
    if(this.password == this.conf_password){
      console.log(this.mail)
      let userDates = {
        email: this.mail,
        password: this.password
      }
      let date_user = {
        name: this.name_user,
        last_name: this.last_name_user,
        grade: this.grade,
      }
      try {
        const result  = await this.afAuth.auth.createUserWithEmailAndPassword(userDates.email, userDates.password);
        if(result) {
          this.afAuth.authState.subscribe(auth => {
            this.afDB.object(`profile/${auth.uid}`).set( date_user )
              .then( ()=> this.navCtrl.setRoot('LoginPage'))
          })
        }
        else {
          console.log('error')
        }
      } catch (e) {
        console.log(e)
      }
    }
  }
  // register() {
  //   if(this.password == this.conf_password){

  //     let loader = this.loadingCtrl.create({
  //       content: "Creando..."
  //     });

  //     loader.present();
  //     let user = {
  //       'mail': this.mail,
  //       'password': this.password,
  //     };
  //     console.log(user.password)
  //     this.servUser.register_user(user)
  //     loader.dismiss()
  //   }
  //   else{
  //     console.log('las contraseñas no coinciden')
  //   }
  // }

}
// interface userDates{
//   mail:string;
//   password:string;
// }
