import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController} from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

import { UserServiceProvider } from '../../providers/user-service/user-service';

@IonicPage()
@Component({
  selector: 'page-aprende',
  templateUrl: 'aprende.html',
})
export class AprendePage {

  datos_user = [];
  mail;
  cards: any;
  category: string = 'gear';
  datos_courses = [];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private toast: ToastController,
              private afAuth: AngularFireAuth,
              public afDB: AngularFireDatabase,
              public userServ: UserServiceProvider) {
              this.cards = new Array(3);
              this.getCourse();
  }
  getCourse() {
    return this.afDB.list(`/course`).valueChanges().subscribe( data => {
      this.datos_courses = data;
      console.log(this.datos_courses)
    })
  }
  ionViewDidLoad() {
    this.afAuth.authState.subscribe(data => {
      if(data.email && data.uid) {
        this.toast.create({
          message:`Bienvenido a EduPlat, ${data.email}`,
          duration: 3000
        }).present();
        this.getUser(data.uid).valueChanges().subscribe( user =>{
          this.datos_user = user;
          this.mail = data.email
          console.log(this.datos_user)
        } )
      }
      else {
        this.toast.create({
          message: 'Fallo en la autenticacion',
          duration: 3000
        }).present();
      }
    })
  }

  getUser(id) {
    return this.afDB.list(`profile/${id}`);
  }


}
