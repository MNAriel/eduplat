
import { Injectable } from '@angular/core';

import { ToastController, } from "ionic-angular";

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { urlToNavGroupStrings } from 'ionic-angular/navigation/url-serializer';

@Injectable()
export class UserServiceProvider {

  private CARPETA_IMAGENES:string = "img";
  private POSTS:string = "eduplat-206af";

  imagenes:any[] = [];
  lastKey:string = undefined;
  publicacion:any = [];

  constructor(
              public afBD: AngularFireDatabase,
              public afAuth: AngularFireAuth,
              private toastCtrl: ToastController) {
    console.log('Hello UserServiceProvider Provider');
  }

  cargar_imagenes_firebase( archivo:archivoSubir ){
    
    let promesa = new Promise( (resolve, reject)=>{

      this.mostrar_toast("Inicio de carga");
      let storageRef = firebase.storage().ref();
      let nombreArchivo = new Date().valueOf(); //1237128371
      
      let uploadTask:firebase.storage.UploadTask =
              storageRef.child(`${ this.CARPETA_IMAGENES  }/${ nombreArchivo }`)
              .putString( archivo.img, 'base64', { contentType: 'image/jpeg' }  );

                    
      uploadTask.on(  firebase.storage.TaskEvent.STATE_CHANGED,
          ( snapshot )=>{}, // saber el avance del archivo
          ( error )=> {  // Manejo de errores

            console.log("Error al subir ", JSON.stringify( error ));
            this.mostrar_toast("Error al cargar: " + JSON.stringify( error ) );
            reject(error);

          },
          ()=>{ // Termino el proceso
            
            //let url = uploadTask.snapshot.downloadURL;
            // let url = uploadTask.snapshot.ref.getDownloadURL().then(function (URL){
            //   // console.log('File available at');
            //   // console.log(URL)
            //   return URL
            // })
            uploadTask.then((snapshot) => {
                snapshot.ref.getDownloadURL().then((url) => {
                    // do something with url here
                    //console.log(url)
                    let URL =  url
                    console.log(url)
                    this.crear_post(  URL, 
                                      archivo.name_course, 
                                      archivo.desp_course, 
                                      archivo.nivel_course,
                                      archivo.teacher_name,
                                      archivo.teacher_last
                                      );
                });
            });
            
            this.mostrar_toast("Imagen cargada exitosamente!!");
            // this.crear_post( URL, 
            //                  archivo.titulo, 
            //                  archivo.descripcion, 
            //                  archivo.mascota_name,
            //                  archivo.name,
            //                  archivo.phone
            //                 );
            
            resolve();

          }
        )

    });
    return promesa;
  }
  private mostrar_toast( texto:string ){

    this.toastCtrl.create({
      message: texto,
      duration: 2500
    }).present();
  }
  private crear_post( url:any,
                      name_course:string, 
                      desp_course:string, 
                      nivel_course:string,
                      teacher_name:string,
                      teacher_last:string){
      
    console.log("llego hasta aqui")                    
    let post:archivoSubir = {
    img: url,
    name_course: name_course,
    desp_course: desp_course,  
    nivel_course: nivel_course,
    teacher_name: teacher_name,
    teacher_last: teacher_last
    };
    console.log(post.img)

    let key = new Date().valueOf(); 

    this.afBD.list(`course`).push( post );
    //this.afBD.object(`course/${key}`).set( post )

    this.imagenes.push( post );

    }
   
}
interface archivoSubir{
  $key?:string;
  img:string;
  name_course:string;
  desp_course:string;
  nivel_course:string;
  teacher_name:string;
  teacher_last:string;
}