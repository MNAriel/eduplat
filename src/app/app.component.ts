import { Component } from '@angular/core';
import { Platform, ToastController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  datos_user = [];
  mail;
  rootPage:string = 'LoginPage';

  constructor(platform: Platform, 
              statusBar: StatusBar, 
              splashScreen: SplashScreen,
              public modalCtrl: ModalController,
              private toast: ToastController,
              private afAuth: AngularFireAuth,
              public afDB: AngularFireDatabase) {
                this.viewDates();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  ionViewDidLoad() {
    this.viewDates();
  }
  viewDates() {
    this.afAuth.authState.subscribe(data => {
      if(data.email && data.uid) {
        this.getUser(data.uid).valueChanges().subscribe( user =>{
          this.datos_user = user;
          this.mail = data.email
          console.log(this.datos_user[0])
        } )
      }
      else {
        this.toast.create({
          message: 'Fallo en la autenticacion',
          duration: 3000
        }).present();
      }
    })
  }

  getUser(id) {
    return this.afDB.list(`profile/${id}`);
  }
  addCourse() {
    let modal = this.modalCtrl.create('CursoPage',{ 'nombre': this.datos_user[2], 'apellido': this.datos_user[1]})
    modal.present();
    modal.onDidDismiss(data => console.log(data));
  }
}

