import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { ImagePicker } from '@ionic-native/image-picker';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { UserServiceProvider } from '../providers/user-service/user-service';

export const firebaseConfig = {
  apiKey: "AIzaSyC9WjyxH7k315SZo_ACrSU6BmtpUOIfCsk",
  authDomain: "eduplat-206af.firebaseapp.com",
  databaseURL: "https://eduplat-206af.firebaseio.com",
  storageBucket: "eduplat-206af.appspot.com",
  messagingSenderId: "667999670194"
};


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserServiceProvider,
    ImagePicker
  ]
})
export class AppModule {}
